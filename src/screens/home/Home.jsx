import './Home.css';
import React from 'react';
import Header from './header/Header'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Post from './post/Post'
import Content from './content/Content'
class Home extends React.Component {
  constructor(props) {
    super(props);
    this.goLogin = this.goLogin.bind(this);
  };
  goLogin() {
    console.log("trang Home")
    console.log(this.props);
    this.props.history.push("/dangnhap");
  }
  render() {
    return (
      <div>
        <Header goLogin={this.goLogin} />
        <Content/>
      
      </div>
    );
  }
}
export default Home;
