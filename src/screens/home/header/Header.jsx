import React from 'react';
import './Header.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Dangtin from '../../../Dangtin';
import  { Link,BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Item from '../../../Item';




class Header extends React.Component {
    
    constructor(props){
        super(props);
        this.goLogin = this.goLogin.bind(this);
    };
    goLogin(){
        console.log("trang login")
        this.props.goLogin();
    }
    render() {
        return (
            <Router>
                <Switch>
                <div>
                    <div class="container">
                        <div class="left">
                            <Link to="/">Phòng trọ</Link>
                        </div>
                        <div class="menu">
                            <ul class="nav_top">
                                <li><Link>Phòng Trọ</Link></li>
                                <li><Link>Nhà,Căn hộ cho thuê</Link></li>
                                <li><Link>Nhà,Căn hộ cho thuê</Link></li>
                            </ul>
                        </div>
                        <div class="box-user">
                            <ul class="nav_user">
                                <li><Link to="/Dangtin">Đăng tin</Link></li>
                                <li><Link to="/dangky">Đăng ký</Link></li>
                                <li><a onClick={this.goLogin}>Đăng nhập</a></li>
                            </ul>
                        </div>
                    </div>
                    <div className="box-search">
                        <div class="quick-search">
                            <form action="post">
                                <input type="text" placeholder="Tìm kiếm" />
                            </form>
                        </div>
                        <div className="search-info">
                            <ul class="dropdown">
                                <li>
                                    <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">
                                        Địa điểm
                            </button>
                                </li>
                                <li>
                                    <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">
                                        Loại
                                </button>
                                    <div className="boxes">
                                        <input type="checkbox" id="check-cho-thue-tro" />
                                        <label htmlFor="check-cho-thue-tro">Phòng Trọ</label>
                                        <input type="checkbox" id="check-cho-thue-tro" />
                                        <label htmlFor="check-cho-thue-tro">Nhà, căn hộ cho thuê</label>
                                        <input type="checkbox" id="check-cho-thue-tro" />
                                        <label htmlFor="check-cho-thue-tro">Tìm ở ghép</label>
                                        <input type="checkbox" id="check-cho-thue-tro" />
                                        <label htmlFor="check-cho-thue-tro">Tìm phòng</label>

                                    </div>
                                </li>
                                <li>
                                    <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">
                                        Giá
                            </button>
                                </li>


                                <div><button class="dropbtn btn-apply" type="button" >
                                    Tìm kiếm
                            </button></div>


                            </ul>


                        </div>

                    </div>

                    

                </div>
                </Switch>
                

            </Router>


        );

    };
}
export default Header;
