
import './App.css';
import React from 'react';
import SignIn from './screens/login/SignIn'
import SignUp from './screens/register/SignUp'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Forget from './screens/forget/Forget';
import Home from './screens/home/Home'

class App extends React.Component {
  render() {
    return (
      <Router>
        <Switch>
          <Route path="/quenmatkhau" component={Forget} />
          <Route path="/dangky" component={SignUp} />
          <Route path="/" exact component={Home} />
          <Route path="/dangnhap" component={SignIn} />
        </Switch>
      </Router>
    );
  }
}
export default App;
