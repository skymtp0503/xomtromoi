import React, { Component } from 'react';

class Dangtin extends Component{
    render(){
        return(
            <div class="module news add">
                <div class="module-header">
                    Thêm tin mới
                    <p class="description">
                        Đăng tin của bạn vào hệ thống tìm trọ
                    </p>
                </div>
                <div class="module-conten">
                    <div class="row">
                        <label>Tiêu đề tin</label>
                        <div class="item long">
                            <input type="text" id="name" name="name" class="" placeholder="Tiêu đề đăng tin"></input>
                        </div>
                    </div>
                    <div class="group-row">
                        <ul>
                        <tr>
                        <th>
                        <div class="row col-3">
                            <label>Loại hình</label>
                            <div class="item long">
                                <select id="type" name="type">
                                    <option value="cho-thue-tro">Cho thuê trọ</option>
    	                            <option value="cho-thue-nha-can-ho">Cho thuê Nhà/Căn hộ</option>
    	                            <option value="tim-o-ghep">Tìm ở ghép</option>
    	                            <option value="tim-tro">Người tìm trọ</option>
                                </select>                              
                            </div>
                        </div>
                        </th><p>&nbsp;</p>
                        <th>
                        <div class="row col-3">
                            <label>Hình thức trọ</label>
                            <div class="item long">
                                <select id="form_type" name="form_type">
                                    <option value="phong-tro">Phòng trọ</option>
    	                            <option value="ky-tuc-xa">Ký túc xá</option>
    	                            <option value="chung-cu-mini">Chung cư mini</option>
                                    <option value="cu-xa">Cư xá</option>
                                    <option value="homestay">Homestay</option>
                                    <option value="tro-nha-nguyen-can">Trọ nhà nguyên căn</option>
                                    <option value="tro-trong-nha-chung-chu">Trọ trong nhà chung chủ</option>
                                </select>                              
                            </div>
                        </div>
                        </th><p>&nbsp;</p>
                        <th>
                        <div class="row col-3">
                            <label>Trọ tự quản</label>
                            <div class="item long">
                                <select id="managing" name="managing">
                                    <option value="0">--- Chọn hình thức ---</option>
    	                            <option value="1">Không</option>
    	                            <option value="2">Có</option>
                                </select>                              
                            </div>
                        </div>
                        </th><p>&nbsp;</p>
                        </tr>
                        </ul>
                    </div>
                    
                    <div class="group-row">
                        <ul>
                        <tr>
                        <th>
                        <div class="row col-3">
                            <label>Phòng trống</label>
                            <div class="item long">
                                <input type="number" id="empty_room" name="empty_room" value="1" placeholder="Phòng còn trống cho thuê" min="0"></input>                           
                            </div>
                        </div>
                        </th><p>&nbsp;</p>
                        <th>
                        <div class="row col-3">
                            <label>Tổng phòng</label>
                            <div class="item long">
                                <input type="number" id="room" name="room" placeholder="Tổng số phòng" min="1"></input>                           
                            </div>
                        </div>
                        </th><p>&nbsp;</p>
                        <th>
                        <div class="row col-3">
                            <label>Ở tối đa (người/phòng)</label>
                            <div class="item long">
                                <input type="number" id="quantity" name="quantity" placeholder="Tối đa số người ở 1 phòng" min="1"></input>                             
                            </div>
                        </div>
                        </th><p>&nbsp;</p>
                        </tr>
                        </ul>
                    </div>
                    <div class="group-row">
                        <ul>
                        <tr>
                        <th>
                        <div class="row col-3">
                            <label>Giá phòng</label>
                            <div class="item long">
                                <input type="number" id="price_min" name="price_min" class="txt_short " min="0" placeholder="Nhập đúng giá trị tiền" title="Giá mỗi tháng"></input>
                                <span class="input-group-addon">VNĐ</span>                           
                            </div>
                        </div>
                        </th><p>&nbsp;</p>
                        <th>
                        <div class="row col-3">
                            <label>Diện tích phòng</label>
                            <div class="item long">
                                <input type="number" id="area-min" name="area-min" placeholder="Diện tích phòng" min="1"></input>
                                <span class="input-group-addon">m²</span>                           
                            </div>
                        </div>
                        </th><p>&nbsp;</p>
                        <th>
                        <div class="row col-3">
                            <label>Giới tính ưu tiên</label>
                            <div class="item long">
                                <select id="gt" name="gt">
    	                            <option value="0">--- Tất cả ---</option>
    	                            <option value="1">Nam</option>
    	                            <option value="2">Nữ</option>
                                </select>                            
                            </div>
                        </div>
                        </th><p>&nbsp;</p>
                        </tr>
                        </ul>
                    </div>
                    <div class="row object_type">
                        <label>Đối tượng</label>
                        <ul class="boxes">
                            <li>
                                <input type="checkbox" id="object_type[study]" name="object_type[study]" value="1"></input>
    							<label for="object_type[study]">Đi học</label>
                            </li>
                            <li>
    							<input type="checkbox" id="object_type[work]" name="object_type[work]" value="1"></input>
    							<label for="object_type[work]">Đi làm</label>
    						</li>
    						<li>
								<input type="checkbox" id="object_type[family]" name="object_type[family]" value="1"></input>
    							<label for="object_type[family]">Gia đình</label>
    						</li>
    						<li>
    							<input type="checkbox" id="object_type[couple]" name="object_type[couple]" value="1"></input>
								<label for="object_type[couple]">Cặp đôi</label>
    						</li>
                        </ul>
                    </div>
                    <div class="row amenities">
    					<label>Tiện nghi</label>
    					<ul class="boxes">
    						<li>
    							<input type="checkbox" id="amenities[mezzanine]" name="amenities[mezzanine]" value="1"></input>
    							<label for="amenities[mezzanine]">Gác lửng</label>
    						</li>
    						<li>
    							<input type="checkbox" id="amenities[wifi]" name="amenities[wifi]" value="1"></input>
    							<label for="amenities[wifi]">Wifi</label>
    						</li>
    						<li>
    							<input type="checkbox" id="amenities[toilet]" name="amenities[toilet]" value="1"></input>
    							<label for="amenities[toilet]">Vệ sinh trong</label>
    						</li>
    						<li>
    			    			<input type="checkbox" id="amenities[bathroom]" name="amenities[bathroom]" value="1"></input>
    							<label for="amenities[bathroom]">Phòng tắm</label>
    						</li>
    						<li>
    							<input type="checkbox" id="amenities[water_heater]" name="amenities[water_heater]" value="1"></input>
    							<label for="amenities[water_heater]">Bình nóng lạnh</label>
    						</li>
    						<li>
    							<input type="checkbox" id="amenities[kitchen_shelf]" name="amenities[kitchen_shelf]" value="1"></input>
    							<label for="amenities[kitchen_shelf]">Kệ bếp</label>
    						</li>
    						<li>
    							<input type="checkbox" id="amenities[washing]" name="amenities[washing]" value="1"></input>
    							<label for="amenities[washing]">Máy giặt</label>
    						</li>
    						<li>
    							<input type="checkbox" id="amenities[tv]" name="amenities[tv]" value="1"></input>
    							<label for="amenities[tv]">Tivi</label>
    						</li>
    						<li>
    							<input type="checkbox" id="amenities[conditioner]" name="amenities[conditioner]" value="1"></input>
    							<label for="amenities[conditioner]">Điều hòa</label>
    						</li>
    						<li>
    							<input type="checkbox" id="amenities[fridge]" name="amenities[fridge]" value="1"></input>
    							<label for="amenities[fridge]">Tủ lạnh</label>
    						</li>
    						<li>
    							<input type="checkbox" id="amenities[pallet]" name="amenities[pallet]" value="1"></input>
    							<label for="amenities[pallet]">Giường nệm</label>
    						</li>
    						<li>
    							<input type="checkbox" id="amenities[wardrobe]" name="amenities[wardrobe]" value="1"></input>
    							<label for="amenities[wardrobe]">Tủ quần áo</label>
    						</li>
    						<li>
    							<input type="checkbox" id="amenities[balcony]" name="amenities[balcony]" value="1"></input>
    							<label for="amenities[balcony]">Ban công/sân thượng</label>
    						</li>
    						<li>
    							<input type="checkbox" id="amenities[elevator]" name="amenities[elevator]" value="1"></input>
    							<label for="amenities[elevator]">Thang máy</label>
    						</li>
    						<li>
    							<input type="checkbox" id="amenities[parking]" name="amenities[parking]" value="1"></input>
    							<label for="amenities[parking]">Bãi để xe riêng</label>
    						</li>
    						<li>
    							<input type="checkbox" id="amenities[camera]" name="amenities[camera]" value="1"></input>
    							<label for="amenities[camera]">Camera an ninh</label>
    						</li>
    						<li>
    							<input type="checkbox" id="amenities[swim_pool]" name="amenities[swim_pool]" value="1"></input>
    							<label for="amenities[swim_pool]">Hồ bơi</label>
    						</li>
    						<li>
    							<input type="checkbox" id="amenities[garden]" name="amenities[parking]" value="1"></input>
    							<label for="amenities[garden]">Sân vườn</label>
    						</li>
    					</ul>
    				</div>
                    <div class="row environment">
    					<label>Môi trường xung quanh</label>
    					<ul class="boxes">
    						<li>
    							<input type="checkbox" id="environment[market]" name="environment[market]" value="1"></input>
    							<label for="environment[market]">Chợ</label>
    						</li>
    						<li>
    							<input type="checkbox" id="environment[supermarket]" name="environment[supermarket]" value="1"></input>
    							<label for="environment[supermarket]">Siêu thị</label>
    						</li>
    						<li>
    							<input type="checkbox" id="environment[hospital]" name="environment[hospital]" value="1"></input>
    							<label for="environment[hospital]">Bệnh viện</label>
    						</li>
    						<li>
    							<input type="checkbox" id="environment[school]" name="environment[school]" value="1"></input>
    							<label for="environment[school]">Trường học</label>
    						</li>
    						<li>
    							<input type="checkbox" id="environment[park]" name="environment[park]" value="1"></input>
    							<label for="environment[park]">Công viên</label>
    						</li>
    						<li>
    							<input type="checkbox" id="environment[bus]" name="environment[bus]" value="1"></input>
    							<label for="environment[bus]">Bến xe Bus</label>
    						</li>
    						<li>
    							<input type="checkbox" id="environment[sport]" name="environment[sport]" value="1"></input>
    							<label for="environment[sport]">Trung tâm thể dục thể thao</label>
    						</li>
    					</ul>
    				</div>
                </div>              
            </div>
        );
    }
}

export default Dangtin;