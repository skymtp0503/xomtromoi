import './SignUp.css';
import React from 'react';
import { Link } from 'react-router-dom';
const axios = require('axios').default;
class SignUp extends React.Component {
  constructor(props) {
    super(props);
    console.log('signup props', props)
    this.state = {
      email: '',
      password: ''
    };
    this.onHandleChange = this.onHandleChange.bind(this);
    this.onHandleSubmit = this.onHandleSubmit.bind(this);
    this.goLogin = this.goLogin.bind(this);
  }

  goLogin() {
    this.props.history.push("/dangnhap")
  }
  onHandleChange(event) {
    var target = event.target;
    var name = target.name;
    var value = target.value;
    this.setState({
      [name]: value
    });
  }
  onHandleSubmit(event) {
    event.preventDefault();
    console.log(this.state);
    axios({
      baseURL: 'http://localhost:4000/',
      method: 'post',
      url: '/register',
      data: {
        email: this.state.email,
        password: this.state.password
      }
    }).then(res => {
      if (res.status === 200) {
        alert('tao account thanh cong')
        this.goLogin();
        return;
      }
    }).catch(error => {
      const status = error.response.status;
      if (status === 422) {
        const message = error.response.data.error;
        alert(message)
      } else if (status === 500) {
        alert("da co loi xay ra vui long thu lai sau")
      }
    });

  }
  render() {
    return (
      <div className="form">
        <form onSubmit={this.onHandleSubmit}>

          <div>
            <input className="input"
              name="tendangnhap"
              type="text"
              placeholder="Tên đăng nhập"
              onChange={this.onHandleChange}
            />
          </div>
          <div>
            <input className="input"
              name="email"
              type="text"
              placeholder="Địa chỉ email"
              onChange={this.onHandleChange}
            />
          </div>
          <div>
            <input className="input"
              name="password"
              type="password"
              placeholder="Mật khẩu"
              onChange={this.onHandleChange}
            />
          </div>
          <div>
            <input className="input"
              type="password"
              placeholder="Nhập lại mật khẩu"
              onChange={this.onHandleChange}
            />
          </div>
          <button type="submit">Đăng ký</button>
        </form>
        <br />
        <p id='chs'>Đã có tài khoản ? - <Link id="linkTo" to="/dangnhap">Đăng nhập</Link></p>

      </div>
    );
  }
}
export default SignUp;
