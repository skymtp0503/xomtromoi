import './SignIn.css';
import React from 'react';
import { Link } from 'react-router-dom';
const axios = require('axios').default;


class SignIn extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: ''
        };
        this.onHandleChange = this.onHandleChange.bind(this);
        this.onHandleSubmit = this.onHandleSubmit.bind(this);
    }
    onHandleChange(event) {
        var target = event.target;
        var name = target.name;
        var value = target.value;
        this.setState({
            [name]: value
        });
    }
    onHandleSubmit(event) {
        event.preventDefault();
        console.log(this.state);
        axios({
            baseURL: 'http://localhost:4000/',
            method: 'post',
            url: '/login',
            data: {
                email: this.state.email,
                password: this.state.password
            }
        }).then(res => {
            alert("Dang nhap thanh cong");
            this.goHome();
            console.log('aaa', res)
        }).catch(error => {
            alert("Email hoac password sai. Vui long nhap lai");
            console.log(error)
        });

    }
    goHome() {
        this.props.history.push("/home")
    }
    render() {
        return ( <
            div className = "form" >


            <
            form onSubmit = { this.onHandleSubmit } >
            <
            div >
            <
            input name = "email"
            className = "input"
            type = "text"
            placeholder = "Địa chỉ Email"
            onChange = { this.onHandleChange }
            /> <
            /div> <
            div >
            <
            input name = "password"
            className = "input"
            type = "password"
            placeholder = "Mật khẩu"
            onChange = { this.onHandleChange }
            /> <
            /div> <
            button type = "submit" > Đăng Nhập < /button> <
            /form> <
            br / >
            <
            Link id = "linkTo1"
            to = "/quenmatkhau" > Quên mật khẩu ? < /Link> <
            Link id = "linkTo2"
            to = "/dangky" > Đăng ký tài khoản < /Link> <
            br / >
            <
            p id = "ch" > Hoặc đăng nhập < /p> <
            br / >

            <
            /div>
        );
    }
}
export default SignIn;