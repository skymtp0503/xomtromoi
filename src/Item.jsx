import React from 'react';
import './App.css';


class Item extends React.Component {
    render() {
        return (
            <div className="item">
                <div className="border"> 
                <div className="img">
                    <a href="https://tromoi.com/" className="itembg" target="blank" >  </a>
                    
                </div>
                <div className="info">
                    <span> Today</span>
                    <h4>Tittle</h4>
                    <div className="price">2.7m/month   </div>
                    <dl> Da Nang</dl>
                </div>
                </div>

            </div>
        );

    };
}
export default Item;
