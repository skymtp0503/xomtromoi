import './Content.css';
import React from 'react';
import Axios from 'axios';


class Content extends React.Component {
    constructor() {
        super();

        this.state = {
            contents: []
        }
    }
    async componentDidMount() {
        await fetch('https://5fd0cf0c1f237400166323cc.mockapi.io/content')
            .then(res => res.json())
            .then((data) => {
                this.setState({ contents: data });
                console.log(data)
            })
            .catch(console.log)
    }

    renderContent = () => {
        let content = this.state.contents.map((data, index) =>
            
                <div className="item">
                    <div className="border">
                        <div className="img">
                            <a href="https://tromoi.com/" className="itembg" target="blank" >  </a>

                        </div>
                        <div className="info">
                            <span> Today</span>
                            <h4>{data.title}</h4>
                            <div className="price">{this.price}   </div>
                            <dl> {data.locate}</dl>
                        </div>
                    </div>

                </div>

            


        );
        return content;
    }

    render() {
        return (
            <div className="contents">
                {this.renderContent()}

            </div>
        );
    }
}
export default Content;